# mpjesbackend

# Backend Test MPJES2
## PREREQUISITOS.
### MS SQL SERVER Express.
CREAR BASE DE DATOS: 

```bash
NOMBRE: directoriompdb.
USUARIO: sa.
PASSWORD: a.
```

SCRIPTS DE CREACION DE ENTIDADES:

Para la creación de las entidades se utilizaron los siguientes scripts:

Entidad Departamentos:

```SQL
USE [directoriompdb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[departamentos](
	[id] [tinyint] NOT NULL,
	[codigo] [varchar](5) NOT NULL,
	[departamento] [varchar](50) NOT NULL,
 CONSTRAINT [PK_departamentos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
```

Entidad Municipios:
```SQL
USE [directoriompdb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[municipios](
	[id] [int] NOT NULL,
	[municipio] [varchar](50) NOT NULL,
	[departamento] [tinyint] NOT NULL,
 CONSTRAINT [PK_municipios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[municipios]  WITH CHECK ADD FOREIGN KEY([departamento])
REFERENCES [dbo].[departamentos] ([id])
GO
```

Entidad Fiscalías:
```SQL
USE [directoriompdb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fiscalias](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[municipio] [int] NOT NULL,
	[fiscalia] [varchar](250) NOT NULL,
	[direccion] [varchar](250) NOT NULL,
	[telefonos] [varchar](250) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[fiscalias]  WITH CHECK ADD FOREIGN KEY([municipio])
REFERENCES [dbo].[municipios] ([id])
GO
```

SCRIPT DE CREACION DE PROCEDIMIENTO ALMACENADO.

Se creó un solo procedimiento almacenado únicamente para cumplir con el requerimiento.

El procedimiento almacenado realiza la consulta de Fiscalías dado un Código de Fiscalía como parámetro.

```SQL
USE [directoriompdb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FISCALIAS_BY_DEPTO]
@CODIGO_DEPTO VARCHAR(5)
AS
BEGIN
SELECT
departamentos.id,
departamentos.codigo,
departamentos.departamento ,
municipios.id AS 'idMunicipio',
municipios.municipio,
municipios.departamento AS 'depto',
fiscalias.id AS 'fiscaliaId',
fiscalias.municipio AS 'municipio_fiscalia',
fiscalias.fiscalia,
fiscalias.direccion,
fiscalias.telefonos
FROM
departamentos
  INNER JOIN municipios ON municipios.departamento = departamentos.id
  INNER JOIN fiscalias ON fiscalias.municipio = municipios.id
WHERE
	departamentos.codigo=@CODIGO_DEPTO

END

GO
```
## REQUISITOS PARA EJECUTAR.
Se utilizó MS SQL SERVER Express, por lo tanto éste servicio debe estar ejecutándose.

El proyecto es un workspace de SPRING BOOT SUITE V.4, por lo tanto debe descargarse SPRING TOOL SUITE V.4.4.4.2 RELEASE, y estar abierto.

El proyecto No está compilado para que pueda cargarse en modo desarrollo y revisar el código utilizado.

Utilice la forma básica de ejecución de una Aplicacion de Spring Boot: (RUN AS: Spring Boot App).
